import Vue from 'vue'
import VueSocialauth from 'vue-social-auth'
import VueAxios from 'vue-axios'
import {$axios} from './axios'

Vue.use(VueAxios, $axios)

// const base = (process.env.NODE_ENV === "production") ? 'https://app.kwizia.com' : 'http://localhost:8081';
const base = (process.env.NODE_ENV === "production") ? 'https://kwizia.com' : 'http://localhost:8081';

Vue.use(VueSocialauth, {

    providers: {
      google: {
        clientId: '465652197097-ahdcurbgimmbepv2qgr3jrnkm3sq051m.apps.googleusercontent.com',
        redirectUri: base+'/auth/google/callback'
      },
      facebook: {
        clientId: '183988890187552',
        redirectUri: base+'/auth/facebook/callback'
      },
    } 
})