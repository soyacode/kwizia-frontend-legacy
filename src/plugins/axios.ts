import axios from "axios";
import store from "@/store";
//import VueAxios from 'vue-axios'

// export default (app: any) => {
//     app.use(VueAxios, axios.create({
//         baseURL: 'http://127.0.0.1:8000',
//         withCredentials: true
//     }));
// }

const instance = axios.create({
  baseURL: (process.env.NODE_ENV === "production") ? "https://app.kwizia.com/" : "http://localhost:8000",
  withCredentials: true,
});

instance.interceptors.request.use((request) => {
  request.headers.common["Accept"] = "application/json";
  request.headers.common["Content-Type"] = "application/json";
  return request;
});

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (
      error.response &&
      [401, 409].includes(error.response.status) &&
      store.getters["auth/isLoggedIn"]
    ) {
      store.dispatch("auth/logout");
    }
    return Promise.reject(error.response);
  }
);

export const $axios = instance;
