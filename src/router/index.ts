import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import Kwiz from '../views/Kwiz.vue'
import Cashier from '../views/Cashier.vue'
import store from '@/store'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
 
  {
    path: '/explore',
    name: 'Explore',
    component: () => import('../views/Explore.vue'),
    meta:{
      needsAuth: true,
    }
  },

  {
    path: '/kwiz/:ref/c',
    component: Kwiz,
    meta:{
      needsAuth: true,
    },
    children: [
      {
        path: '',
        name: 'SingleCampaign',
        component: () => import('../views/kwiz/SingleCampaign.vue')
      },
      {
        path: 'choose-level/:campaign_id/:quizable_id',
        name: 'ChooseLevel',
        component: () => import('../views/kwiz/ChooseLevel.vue')
      },
      {
        path: 'create-session/:token',
        name: 'CreateSession',
        component: () => import('../views/kwiz/CreateSession.vue')
      }
    ]
  },

  {
    path: '/my-account',
    component: () => import('../views/Settings.vue'),
    meta:{
      needsAuth: true,
    },
    children: [
      {
        path: '',
        name: 'MyAccount',
        component: () => import('../views/user/Index.vue')
      },
      {
        path: 'email',
        name: 'EmailSetting',
        component: () => import('../views/user/EmailSetting.vue')
      },
      {
        path: 'password',
        name: 'PasswordSetting',
        component: () => import('../views/user/PasswordSetting.vue')
      }
    ]
  },

  {
    path: '/kwiz-history',
    name: 'KwizHistory',
    component: () => import('@/views/KwizHistory.vue'),
    meta:{
      needsAuth: true,
    }
  },

  {
    path: '/cashier',
    component: Cashier,
    children:[
      {
        path: '',
        name: 'Cashier',
        component: () => import('../views/cashier/Index.vue'),
      },
      {
        path: '/withdraw',
        name: 'Withdraw',
        component: () => import('../views/cashier/Withdraw.vue'),
      },
      {
        path: '/add-fund',
        name: 'Deposit',
        component: () => import('../views/cashier/Deposit.vue'),
      },
      {
        path: '/verify-pay',
        name: 'PaymentNotice',
        component: () => import('../views/cashier/PaymentNotice.vue'),
      },
    ],
    meta:{
      needsAuth: true,
    }
  },

  {
    path: '/how-to-play',
    name: 'HowToPlay',
    component: () => import('../views/HowToPlay.vue'),
    meta:{
      needsAuth: true,
    }
  },

  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue'),
    meta:{
      needsAuth: true,
    }
  },


  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue'),
    meta:{
      needsAuth: false,
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/Register.vue'),
    meta:{
      needsAuth: false,
    }
  },
  {
    path: '/auth/:provider/callback',
    component: {
      template: '<div class="auth-component"></div>'
    }
  },
  {
    path: '/play/:session_id',
    name: 'Trivia',
    component: () => import('../views/Trivia.vue'),
    meta:{
      needsAuth: true,
    }
  },
  {
    path: '/result/:session_id',
    name: 'Result',
    component: () => import('../views/Result.vue'),
    meta:{
      needsAuth: true,
    }
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const isLoggedIn = store.getters["auth/isLoggedIn"];
  const reqAuth = to.matched.some((record) => record.meta.needsAuth);
  const redirectUrl = { name: "Login", query: { redirect: to.fullPath } };
  if (reqAuth && !isLoggedIn) {
    store.dispatch("auth/me").then(() => {
      if (!store.getters["auth/isLoggedIn"]) {
        next(redirectUrl);
      } else {
        next();
      }
    });
  } else {
    if (isLoggedIn && !store.getters["auth/user"]) {
      store.dispatch("auth/me").then(() => {
        if (!store.getters["auth/isLoggedIn"]) {
          next(redirectUrl);
        } else {
          next();
        }
      });
    } else {
      next();
    }
  }
});


export default router
